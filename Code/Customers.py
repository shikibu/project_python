import pandas as pd
import os
import numpy as np
import random
from itertools import chain


# to pick a random element in a list with different probabilities given in another list
def random_pick(some_list, probabilities):
    x = random.uniform(0, 1)
    cumulative_probability = 0.0
    for item, item_probability in zip(some_list, probabilities):
        cumulative_probability += item_probability
        if x < cumulative_probability:
            break
    return item


df = pd.read_csv(os.path.abspath('../Data/Coffeebar_2013-2017.csv'), sep=";")
df = pd.DataFrame(df)
df['FOOD'] = df['FOOD'].fillna("NoFood")

# we create a list with drinks and their respective prices
listdrinks = df['DRINKS'].unique()

listfood = df['FOOD'].unique()

dataframedrinksprices = pd.DataFrame(({"coffee": [3], "frappucino": [4], "milkshake": [5],
                                       "soda": [3], "tea": [3], "water": [2]}), index=["prices"])

dataframefoodprices = pd.DataFrame(({"NoFood": [0], "cookie": [2], "muffin": [3], "pie": [3],
                                     "sandwich": [5]}), index=["prices"])

# we upload our food probability table
dfood = pd.read_csv(os.path.abspath('./food.csv'))
dfood = pd.DataFrame(dfood)


# we upload our drinks probability table
ddrinks = pd.read_csv(os.path.abspath('./drinks.csv'))
ddrinks = pd.DataFrame(ddrinks)


class Customer(object):
    def __init__(self, c_id):
        self.c_id = c_id

    def buy(self, time):
        food_probabilities = dfood.loc[dfood['ordertime'] == time[-8:]].values
        drinks_probabilities = ddrinks.loc[ddrinks['ordertime'] == time[-8:]].values

        # to transform array into a list and delete its first element
        npa = np.asarray(food_probabilities)
        npa2 = np.asarray(drinks_probabilities)
        npa = npa.reshape((6, 1))
        npa2 = npa2.reshape((7, 1))
        food_probabilities = list(chain.from_iterable(npa))
        drinks_probabilities = list(chain.from_iterable(npa2))

        del food_probabilities[0]
        del drinks_probabilities[0]

        item = random_pick(listdrinks, drinks_probabilities)
        print(item)
        price = dataframedrinksprices.loc["prices", item]
        self.budget = self.budget - price

        # to pick a random element in a list with different probabilities given in another list
        item2 = random_pick(listfood, food_probabilities)
        print(item2)
        price = dataframefoodprices.loc["prices", item2]
        self.budget = self.budget - price
        return item, item2


class Returning(Customer):
    def __init__(self, c_id):
        Customer.__init__(self, c_id)
        self.history = pd.DataFrame()

    def buy(self, time):
        food_probabilities = dfood.loc[dfood['ordertime'] == time[-8:]].values
        drinks_probabilities = ddrinks.loc[ddrinks['ordertime'] == time[-8:]].values

        # to transform array into a list and delete its first element
        npa = np.asarray(food_probabilities)
        npa2 = np.asarray(drinks_probabilities)
        npa = npa.reshape((6, 1))
        npa2 = npa2.reshape((7, 1))
        food_probabilities = list(chain.from_iterable(npa))
        drinks_probabilities = list(chain.from_iterable(npa2))

        del food_probabilities[0]
        del drinks_probabilities[0]

        item = random_pick(listdrinks, drinks_probabilities)
        print(item)
        price = dataframedrinksprices.loc["prices", item]
        self.budget = self.budget - price

        # to pick a random element in a list with different probabilities given in another list
        item2 = random_pick(listfood, food_probabilities)
        print(item2)
        price = dataframefoodprices.loc["prices", item2]
        self.budget = self.budget - price
        self.history = self.history.append(pd.DataFrame(({"Food": [item], "Drinks": [item2]}), index=[time]))
        return item, item2

class Hipster(Returning):
    def __init__(self, c_id):
        Returning.__init__(self, c_id)
        self.budget = 500


class RRegulars(Returning):
    def __init__(self, c_id):
        Returning.__init__(self, c_id)
        self.budget = 250


class OneTime(Customer):
    def __init__(self, c_id):
        Customer.__init__(self, c_id)
        self.budget = 100


class OTRegulars(OneTime):
    def __init__(self, c_id):
        OneTime.__init__(self, c_id)
        self.budget = self.budget


class TripAdvisor(OneTime):
    def __init__(self, c_id):
        OneTime.__init__(self, c_id)
        self.tip = (random.randint(0, 10))
        self.budget = self.budget - self.tip