# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Exploratory###

* In the exploratory file, we compute the answers for the first part of the problem set. 
* Also in this script we create two new csv files containing the probabilites of consumption for both drink and food
* as it will be easier and cleaner to recall this data in futures modules.

### Customer###

* In this file, we create the customers that will visit the coffee bar simulation

### Sim ###

* 5 years coffee bar simulation
