import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import os

# reading csv file and creating a data frame from it
df = pd.read_csv(os.path.abspath('../Data/Coffeebar_2013-2017.csv'), sep=";")

df = pd.DataFrame(df)

print(df)

# replace the missing values in Food column
df['FOOD'] = df['FOOD'].fillna("NoFood")

# drinks offered at the bar
listdrinks = df['DRINKS'].unique()
print("The bar offers to drink: ")
print(listdrinks)


# food offered at the bar
listfood = df['FOOD'].unique()
print("The bar offers to eat: ")
print(listfood)

# description dataframe
print(df.describe())

# plot histogram drinks
print(df['DRINKS'].value_counts())
df['DRINKS'].value_counts().plot(kind='bar')
plt.show()

# plot histogram food
print(df['FOOD'].value_counts())
df['FOOD'].value_counts().plot(kind='bar')
plt.show()

# we create an extra column in which we extract the hour in which the customer bought at the coffee bar
list1 = (df['TIME'])
list2 = []
i = " "
for i in list1:
    list2.append(i[-8:])   # takes the 8 last characters of an element

# we create a ne variable containing the minutes#
df['ordertime'] = list2
print(df)

# we created two pivot tables to look the consumption of drinks and food per hour
print(pd.pivot_table(df, index=["ordertime"], columns=['DRINKS'], values=["TIME"], aggfunc=[len], margins=True))
print(pd.pivot_table(df, index=["ordertime"], columns=['FOOD'],  values=["TIME"], aggfunc=[len], fill_value=0,
                     margins=True))

# probabilities calculations
probafood = pd.crosstab(df["ordertime"], df["FOOD"], normalize='index')
print(probafood)
probadrink = pd.crosstab(df["ordertime"], df["DRINKS"], normalize='index')
print(probadrink)

# we create a new csv file with the probabilities of food consumption
csvfood = pd.DataFrame(probafood)
csvfood.to_csv("./food.csv", index=True)


# we create a new csv file with the probabilities of drinks consumption
csvdrinks = pd.DataFrame(probadrink)
csvdrinks.to_csv("./drinks.csv",index=True)

