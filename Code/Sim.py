import numpy as np
import random
import pandas as pd
from datetime import date, datetime, timedelta
from Customers import df, TripAdvisor, OTRegulars, Hipster, RRegulars, random_pick, dfood, ddrinks, Customer, Returning
from itertools import chain



Simulation = pd.DataFrame({"Time": [], "Customer type": [], "Food": [], "Drinks": []})
Customerlist = []

# random selection of 1000 returning customer id form initial data frame
uniqueCID = df['CUSTOMER'].unique()
returningCID = np.random.choice(uniqueCID, 1000)

# to transform array into a list
npa = np.asarray(returningCID)
npa = npa.reshape((1000, 1))
listreturningCID = list(chain.from_iterable(npa))

# creates 1000 returning customers 1/3 of Hipsters 2/3 of RRegulars from the returningCID list
listReturning = ['Hipster', 'RRegular']
Returning_probabilities = [0.333, 0.666]

for elt in returningCID:
    G = random_pick(listReturning, Returning_probabilities)

    if G == "Hipster":
        C = Hipster(elt)
    else:
        C = RRegulars(elt)
    Customerlist.append(C)

# Generation of a unique customer id for the 4000 one time customers
def random_with_N_digits(n):
    range_start = 10**(n-1)
    range_end = (10**n)-1
    return random.randint(range_start, range_end)


def uniqueid():
    while 1:
        id = "CID"+str(random_with_N_digits(8))
        if id not in listreturningCID:
            listreturningCID.append(id)
            break
        else:
            id = "CID"+str(random_with_N_digits(8))
    return id


# creates 4000 OneTime customers : 10% of TripAdvisor 90% of OTRegular
listOneTime = ['OTRegular', 'TripAdvisor']
OneTime_probabilities = [0.9, 0.1]
x = 0
while x < 4000:
    G = random_pick(listOneTime, OneTime_probabilities)

    if G == "OTRegular":
        C = OTRegulars(uniqueid())
    else:
        C = TripAdvisor(uniqueid())
    x += 1
    Customerlist.append(C)


# time simulation
def mytime(begin, end, delta):
    current = begin
    if not isinstance(delta, timedelta):
        delta = timedelta(**delta)
    while current < end:
        yield current
        current += delta


# coffee shop life simulation
def simulate():
    begin = datetime(2018, 1, 1)
    end = datetime(2018, 12, 31)
    pocframe = pd.DataFrame()
    for d in mytime(begin, end, {'minutes': 1}):
        t = str(d)
        if int("8") <= int(t[-8:-6]) < int("11"):
            if int(t[-5:-3]) % 5 == 0:
                Client = np.random.choice(Customerlist)
                while 1:
                    if Client.budget > 10:  # to always have a client buying otherwise he will not buy
                        it = Client.buy(t)
                        f = it[0]
                        d = it[1]
                        pocframe = pocframe.append(pd.DataFrame(({"CID": Client.c_id, "CTYPE": type(Client), "FOOD": f, "DRINKS": d}), index=[t]))
                        pocframe.to_csv("./pocframe.csv", sep=';', index=True)
                        print(t)
                        break
                    else:
                        Client = np.random.choice(Customerlist)

        elif int("11") <= int(t[-8:-6]) < int("13"):
            if int(t[-5:-3]) % 2 == 0:
                Client = np.random.choice(Customerlist)
                while 1:
                    if Client.budget > 10:
                        it = Client.buy(t)
                        f = it[0]
                        d = it[1]
                        pocframe = pocframe.append(pd.DataFrame(({"CID": Client.c_id, "CTYPE": type(Client), "FOOD": f, "DRINKS": d}), index=[t]))
                        pocframe.to_csv("./pocframe.csv", sep=';', index=True)
                        print(t)
                        break
                    else:
                        Client = np.random.choice(Customerlist)

        elif int("13") <= int(t[-8:-6]) < int("18"):
            if int(t[-5:-3]) % 4 == 0:
                Client = np.random.choice(Customerlist)
                while 1:
                    if Client.budget > 10:
                        it = Client.buy(t)
                        f = it[0]
                        d = it[1]
                        pocframe = pocframe.append(pd.DataFrame(({"CID": Client.c_id, "CTYPE": type(Client), "FOOD": f, "DRINKS": d}), index=[t]))
                        pocframe.to_csv("./pocframe.csv", sep=';', index=True)
                        print(t)
                        break
                    else:
                        Client = np.random.choice(Customerlist)

simulate()


# Creates 2 returning clients in order to show their history
Georges = np.random.choice(Customerlist)
Carl = np.random.choice(Customerlist)

while 1:
     if isinstance(Georges, RRegulars):
            print("Georges History")
            print(Georges.history)
            print("Georges budget")
            print(Georges.budget)
            break
     else:
            Georges = np.random.choice(Customerlist)

while 1:
     if isinstance(Carl, Hipster):
            print("Carl History")
            print(Carl.history)
            print("Carl budget")
            print(Carl.budget)
            break
     else:
            Carl = np.random.choice(Customerlist)
